"""
This is a template file that shows how to implement main() in Python.
Author: Omer Rosenbaum
"""


def main():
    print("You had me at Hello World")

	
if __name__ == '__main__':
    # TODO: Parse arguments from the user here
    main()
