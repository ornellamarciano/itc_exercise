def encode_title(word, pad_width):
    return word.title().zfill(pad_width)


def main():
    word_to_encode = input("Please enter a string to encode : ")
    min_width = int(input("Please enter the minimum length : "))
    print(encode_title(word_to_encode, min_width))

if __name__ == '__main__':
    main()