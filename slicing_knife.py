WORD_CONSTANT = "We were more than just a slice"

def slicing_knife(word):
    print(word[0:6])
    print(word[1:-2])
    print(word[::2])
    print(word[3:-2:2])
    print(word[::-1])


slicing_knife(WORD_CONSTANT)