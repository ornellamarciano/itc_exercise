def multiplier(word):
    multiple = 2
    str_multiplied = ""
    for i in range(len(word)):
        str_multiplied += word[i] * multiple
    return str_multiplied


def main():
    user_word = input("Please enter a string : ")
    print(multiplier(user_word))


if __name__ == '__main__':
    main()
