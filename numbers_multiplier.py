def numbers_multiplier(list_numbers):
    result = list_numbers[0]
    for i in range(1, len(list_numbers)):
        result *= list_numbers[i]
    return result


def main():
    user_numbers = input("Please enter a list of numbers delimited by a space : ")
    user_numbers_list = user_numbers.split(" ")
    for i in range(len(user_numbers_list)):
        user_numbers_list[i] = int(user_numbers_list[i])
    print(numbers_multiplier(user_numbers_list))

if __name__ == '__main__':
    main()